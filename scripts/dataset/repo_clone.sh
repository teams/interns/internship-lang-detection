#!/bin/bash

# This script uses git clone to get head version of git repositories.

ROOT_FOLDER=$1
OLDIFS=$IFS
IFS=$'\n'

if [ $# -eq 1 ]
then
    for i in $( ls "../../dataset/repo_lists" -1 );
    do
	    for url in $( cat "../../dataset/repo_lists/$i" );
	    do
	        USER_NAME="$(basename $(dirname $url))";
	        REPO_NAME="$(basename "$url" .git)";
	        PREFIX=$(echo "${USER_NAME:0:2}" | tr '[:upper:]' '[:lower:]');
	        git clone --depth=1 $url "$ROOT_FOLDER/$PREFIX/$USER_NAME>$REPO_NAME";
	    done
    done
else
    echo "Please enter root folder correctly. One folder is needed."
fi

IFS=$OLDIFS
