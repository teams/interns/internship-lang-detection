#!/bin/bash

ROOT_FOLDER=$1
OLDIFS=$IFS
IFS=$'\n'

if [ $# -eq 1 ]
then
    mkdir $ROOT_FOLDER/../ground_truth_text/;
    cd /Users/yinyuan/Desktop/TRE/linguist;
    
    for root in $( ls $ROOT_FOLDER -1 );
    do
	for repo in $( ls "$ROOT_FOLDER/$root" -1 );
	do
            GROUND_TRUTH=$ROOT_FOLDER/../ground_truth/$root/$repo.json;
	    HE=$(head -c 1 $GROUND_TRUTH);
	    if [ $HE = '{' ];
	    then
	        :
	    else
		mkdir "$ROOT_FOLDER/../ground_truth_text/$root";
		echo "Generating text description of '$repo'";
		bundle exec linguist "$ROOT_FOLDER/$root/$repo" --breakdown | grep -ax '.*' > $ROOT_FOLDER/../ground_truth_text/$root/$repo;
	    fi
	done
    done
else
    echo "Please enter root folder correctly. One folder is needed."
fi

IFS=$OLDIFS
