import os
import sys
import io
import json

from github import Github, PaginatedList
from time import sleep
from yaml import load

def main():
    with open('../../swh/langdetect/static_data/languages.yml', 'r') as f:
        ls = load(f)
        languages = list(ls.keys())
        for language in languages:
            get_repos(language)

def get_repos(language):
    total = 75
    token = '9c0ec844e5e9293c8e64f49892c15ff1e8a923d8'
    g = Github(token, per_page = total)
    print('Fetching {!r} repository list for GitHub..'.format(language))
    repos = g.search_repositories('language:"'+language+'" size:<150000', 'stars', 'desc')
    if repos.totalCount == 0:
        print('Empty list returned for {!r}, ignored.'.format(language))
        return
    else:
        repo_list = '../../dataset/repo_lists/' + language
        with open(repo_list, 'w') as f:
            if repos.totalCount < total:
                total = repos.totalCount
            for repo in repos[0:total] :
                f.write(repo.clone_url + '\n')
    sleep(2.5)
    print('Completed!')


if __name__ == '__main__':
    main()

