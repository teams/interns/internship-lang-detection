"""
Langdetect detects the programming language of source code file.

"""

from .cnn import CNN

__cnn_classifer = CNN(None, 4096, None)

def classify(path):
    __cnn_classifer.classify(path)
    
