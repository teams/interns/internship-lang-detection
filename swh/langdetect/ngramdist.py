import os
import sys
import time
import random
import csv
import json
import argparse
import nltk
import operator

from ast import literal_eval
from itertools import islice
from pickle import dump, load
from nltk.util import ngrams
from .utils.common import Tokenizer, file_to_string, find_file, count_files

csv.field_size_limit(sys.maxsize)

def main():
    parser = argparse.ArgumentParser(description='Training and test tool of frequency distance of n-grams.')

    subparsers = parser.add_subparsers(dest='sub_command')

    parser_train = subparsers.add_parser('train', help='Training on the dataset, dataset must be a *.csv file. A model will be created in the same directory.')
    parser_train.add_argument('train_path', metavar='PATH', type=str, help='Path of the training dataset.')
    # parser_train.add_argument('-n', '--ngrams',  metavar='N', dest='train_maxsize', type=int, help='Set maximum input size of ConvNet, default 5.')
    parser_test = subparsers.add_parser('test', help='Test on the dataset, dataset must be a directory with *.csv dataset named by corresponding language.')
    parser_test.add_argument('test_root', metavar='ROOT', type=str, help='Root of the test dataset.')
    
    if len(sys.argv[1:]) == 0:
        parser.print_help()
        parser.exit()
    args = parser.parse_args()
    
    if args.sub_command == 'train' :
        n = NGramDist(args.train_path)
        n.train()
    elif args.sub_command == 'test':
        n = NGramDist(args.test_root)
        n.test()
    else:
        parser.parse_args('-h')

class NGramDist:

    def __init__(self, path):
        
        self._path = path

        # Root of model folder
        self._root_model = os.path.join(os.path.dirname(path), 'model_ngram_dist')
        try:
            os.mkdir(self._root_model)
        except:
            pass

        # Path of result
        self._path_result = os.path.join(os.path.dirname(path), 'result_ngram_dist')

        dir_path = os.path.dirname(os.path.abspath(__file__))
        with open(os.path.join(dir_path, 'static_data', 'languages.json'), 'r') as f:
            self._languages = json.load(f)

        self._path_test_csv = path

        self._num_of_classes = len(self._languages)

    def file_len(self, fname):
        with open(fname) as f:
            count = 0
            for l in f:
                count += 1
            return count

    def train(self):
        statistics = {}
        t_start = time.perf_counter()
        with open(self._path, newline='') as csvfile:
            r = csv.reader(csvfile, delimiter=' ', quotechar='|')
            for pair in r:
                label, string = pair
                label = int(label)
                language = self._languages[label]
                statistics_lang = statistics.get(language, {})
                
                string = literal_eval(string)
                tokens = Tokenizer.tokenize(string, 'letter')
                generated_ngrams = self._generate_ngrams([chr(token) for token in tokens], 3)

                #tokens = Tokenizer.tokenize(string, 'word')
                #tokens = [''.join([chr(x) for x in token]) for token in tokens]
                #generated_ngrams = self._generate_ngrams(tokens, 3)
                
                self._count_ngrams(statistics_lang, generated_ngrams)
                statistics[language] = statistics_lang
                
                t_end = time.perf_counter()
                print(str(t_end - t_start) + '   ' + str(label), end='\r')

        for language in self._languages:
            with open(os.path.join(self._root_model, language), 'wb') as f:
                dump(self._sort_by_value(statistics[language]), f)

    def _generate_ngrams(self, tokens, n):
        generated_ngrams = []

        for i in range(1, n+1):
            igrams = ngrams(tokens, i,
                            pad_left=True,
                            pad_right=True,
                            left_pad_symbol  = '$BOF$',
                            right_pad_symbol = '$EOF$')
            for igram in igrams:
                generated_ngrams.append(''.join(igram))

        return generated_ngrams

    def _count_ngrams(self, statistics, ngrams):
        for ngram in ngrams:
            statistics[ngram] = statistics.get(ngram, 0) + 1
                        
    def test(self):
        try:
            r = open(self._path_result, 'rb')
            test_result = load(r)
            r.close()
        except FileNotFoundError:
            test_result = {}
            
        model = self._load_models()
        
        for language in [x for x in self._languages if x not in test_result.keys()]:
            test_result[language] = self.test_class(model, language)
            with open(self._path_result, 'wb') as f:
                dump(test_result, f)

    def _load_models(self):
        models = {}
        
        for model in [model
                      for model in self._languages]:
            root_model = os.path.join(self._root_model, model)
            with open(root_model, 'rb') as sorted_file:
                models[model] = self._list_to_dict(load(sorted_file))
                
        return models

    def _list_to_dict(self, model):
        model_ngrams = [x[0] for x in model]
        model_dict = {}
        index = 0
        for ngram in model_ngrams:
            index += 1
            model_dict[ngram] = index
        return model_dict
    
    def _count_size(self, files):
        size = 0
        for f in files:
            size += os.path.getsize(f)
        return size
    
    def test_class(self, model, language):
        ok = 0
        results = []
        count = 0
        total_test = self.file_len(os.path.join(self._path_test_csv, language + '.csv'))

        t_start = time.perf_counter()
        with open(os.path.join(self._path_test_csv, language + '.csv'), newline='') as csvfile:
            r = csv.reader(csvfile, delimiter=' ', quotechar='|')
            for pair in r:
                label, string = pair
                label = int(label)
                string = literal_eval(string)
                result = self._guess_file_language(model, string)
                count += 1
                results.append(result[0])
                if result[0][1] == language:
                    ok += 1
                t_end = time.perf_counter()
                print('[{0:4d}/{1:4d}] {2}:{3}   {4}    '.format(count, total_test, result[0][1], result[0][0], t_end - t_start), end='\r')

        accuracy = ok / total_test
        print('Tests for {}                   '.format(language))
        print('Total test files           : {}'.format(total_test))
        print('Correctly classified files : {}'.format(ok))
        print('Accuracy                   : {}%'.format(accuracy * 100))
        return (ok, total_test, accuracy, results)

    def speed_benchmark(self):
        language = self._languages[10]
        model = self._load_model()

        test_set = self._get_test_set(language)
        total_size = self._count_size(test_set)
        print('{} kB in total'.format(total_size / 1024))
        
        t_start = time.perf_counter()
        self.test_class(model, language)
        t_end = time.perf_counter()
        
        print('{} seconds.'.format(t_end - t_start))
        print('{} seconds per KiB'.format(((t_end - t_start) / total_size) * 1024))

    def _guess_file_language(self, models, string):
        tokens = Tokenizer.tokenize(string, 'letter')
        generated_ngrams = self._generate_ngrams([chr(token) for token in tokens], 3)

        #tokens = Tokenizer.tokenize(string, 'word')
        #tokens = [''.join([chr(x) for x in token]) for token in tokens]
        #generated_ngrams = self._generate_ngrams(tokens, 3)
        
        statistics = {}
        self._count_ngrams(statistics, generated_ngrams)
        
        test_profile = self._list_to_dict(self._sort_by_value(statistics))

        result = []

        for model in models.keys():
            root_model = os.path.join(self._root_model, model)
            model_profile = models[model]
            distance = self._distance(model_profile, test_profile)
            result.append((distance, model))

        return sorted(result)

    def _sort_by_value(self, statistics):
        statistics_sorted = sorted(statistics.items(),
                                   key = operator.itemgetter(1),
                                   reverse = True)[:500]
        return statistics_sorted

    def _distance(self, model_profile, test_profile):
        distance = 0
        maximum = len(test_profile)

        for test_ngram in test_profile.keys():
            test_rank = test_profile.get(test_ngram)
            model_rank = model_profile.get(test_ngram, maximum)
            d = abs(test_rank - model_rank)
            distance += d

        return distance

if __name__ == '__main__':
    main()
