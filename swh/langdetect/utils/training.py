import os
import random
import csv
import json

from .common import count_files, find_file, file_to_string
from shutil import copyfile


class Dataset:

    def __init__(self, root):
        self.root_code = os.path.join(root, '..', 'code_by_language')
        self.root_training = os.path.join(root, '..', 'training_set')
        self.root_training_csv = os.path.join(root, '..', 'training_set_csv')
        self.root_test = os.path.join(root, '..', 'test_set')
        self.root_test_csv = os.path.join(root, '..', 'test_set_csv')
        try:
            os.mkdir(self.root_training)
        except FileExistsError:
            pass
        try:
            os.mkdir(self.root_training_csv)
        except FileExistsError:
            pass
        try:
            os.mkdir(self.root_test)
        except FileExistsError:
            pass
        try:
            os.mkdir(self.root_test_csv)
        except FileExistsError:
            pass
        dir_path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        print(dir_path)
        with open(os.path.join(dir_path, 'static_data', 'languages_mini.json'), 'r') as f:
            self._languages = json.load(f)
    
    def build_training_set(self):
        for language in self._languages:
        # limit defines the size of training set
        # upper defines the maximum size
            root_code_language = os.path.join(self.root_code, language)
            root_training_language = os.path.join(self.root_training, language)
            total = count_files(root_code_language)
            try:
                os.mkdir(root_training_language)
            except FileExistsError:
                pass

            upper = 1000
            if total >= upper:
                limit = upper // 2
            else:
                limit = total // 2
            
            indices = random.sample(range(1, total + 1), limit)
            files = map(lambda x : find_file(root_code_language, x), indices)
            for src in files:
                basename = os.path.basename(src)
                des = os.path.join(root_training_language, basename)
                os.symlink(src, des)

    def build_test_set(self, extension=True):
        for language in self._languages:
            root_language = os.path.join(self.root_code, language)
            root_test_language = os.path.join(self.root_test, language)
            try:
                os.mkdir(root_test_language)
            except FileExistsError:
                pass

            files = self.get_test_set(language)
            for src in files:
                if extension:
                    des = os.path.join(root_test_language, os.path.basename(src))
                else:
                    des = os.path.join(root_test_language, os.path.splitext(os.path.basename(src))[0])
                os.symlink(src, des)

    def train_files_with_label(self):
        with open(os.path.join(self.root_training_csv, 'training_set.csv'), 'w', newline='') as csvfile:
            setwriter = csv.writer(csvfile, delimiter=' ', quotechar='|', quoting=csv.QUOTE_MINIMAL)
            lang_index = {k : v for v, k in enumerate(self._languages)}
            for language in self._languages:
                print(language)
                root_training_language = os.path.join(self.root_training, language)
                index_lang = lang_index[language]
                for f in [x for x in os.listdir(root_training_language) if not x.startswith('.')]:
                    filename = os.path.join(root_training_language, f)
                    _, extension = os.path.splitext(f)
                    text = extension.encode() + b' ' +  file_to_string(filename) # 10240
                    setwriter.writerow([index_lang, text])

    def get_test_set(self, language):
        root_training_language = os.path.join(self.root_training, language)
        root_language = os.path.join(self.root_code, language)
        total = count_files(root_language)
        training_set = [int(os.path.splitext(x)[0]) for x in os.listdir(root_training_language) if not x.startswith('.')]
        
        it = [find_file(root_language, x) for x in range(1, total + 1) if x not in training_set]
        try:
            test_set = random.sample(it, 1000)
        except ValueError:
            test_set = it
            
        if len(test_set) == 0:
            it = [find_file(root_language, x) for x in range(1, total + 1) if x not in training_set]
            try:
                test_set = random.sample(it, 1000)
            except ValueError:
                test_set = it
        return test_set

    def test_files_with_label(self):
        for language in self._languages:
            root_test_language = os.path.join(self.root_test, language)
            index_lang = self._languages.index(language)
            with open(os.path.join(self.root_test_csv, language + '.csv'), 'w', newline='') as csvfile:
                setwriter = csv.writer(csvfile, delimiter=' ', quotechar='|', quoting=csv.QUOTE_MINIMAL)
                for f in [x for x in os.listdir(root_test_language) if not x.startswith('.')]:
                    filename = os.path.join(root_test_language, f)
                    _, extension = os.path.splitext(f)
                    text = extension.encode() + b' ' + file_to_string(filename)
                    setwriter.writerow([index_lang, text])
