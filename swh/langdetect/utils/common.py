"""
Here regroup basic preprocessing methods
used in learning stage for different 
approaches.

"""

import re, os, time

_not_start_with_point = lambda x: not x.startswith('.')

class Tokenizer():

    separator = re.compile(
        b'([\x20-\x2f\x3a-\x40\x5b-\x5e\x60\x7b-\x7e\s]|\d+\.\d+|\d+|\d+\.\d+[eE][+-]?\d+)')

    def is_number(n):
        try:
            float(n)
        except ValueError:
            return False
        return True

    def tokenize(text, re_name):
        ''' Splits text into tokens '''
        if re_name == 'letter':
            return list(text)
        elif re_name == 'word':
            pretokens = [x for x in Tokenizer.separator.split(text) if x ]
            tokens = []
            for x in pretokens :
                if Tokenizer.is_number(x):
                    tokens.append(b'<number>')
                elif x.isspace() and x != b'\n':
                    tokens.append(b' ')
                else:
                    tokens.append(x)
            return tokens
                    

def file_to_string(filename):
    """ Read a file to a string. """
    with open(filename, 'rb') as f:
        data = f.read()
    return data

def count_files(root_language):    
    all_folders = natural_sort(filter
                               (_not_start_with_point,
                                os.listdir(root_language)))
    files = natural_sort(filter
                         (_not_start_with_point,
                          os.listdir(root_language + '/' + all_folders[-1])))
    (max,_) = os.path.splitext(files[-1])
    return int(max)

def find_file(root_language, n):
    '''Find the n-th file in language folder'''
    if n > count_files(root_language):
        return ''
    else:
        start = (n - 1) // 1000 * 1000 + 1
        end = start + 999
        root_count = root_language + '/' + str(start) + '-' + str(end)
        files = natural_sort(filter
                             (_not_start_with_point,
                              os.listdir(root_count)))
        return root_count + '/' + files[n - start]

'''def replace_string_and_number(text):
    """ Replace strings and numbers in a file by special tokens 
    """
    str_replaced = _re_string.sub(b'"__str__"', text)
    str_num_replaced = _re_number.sub(b'__num__', str_replaced)
    #str_num_replaced = text
    return str_num_replaced
'''

def natural_sort(l): 
    convert = lambda text: int(text) if text.isdigit() else text.lower() 
    alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ] 
    return sorted(l, key = alphanum_key)

def remove_comment(binary_text, language):
    splited_text = binary_text.splitlines()
    text = b'\n'.join(splited_text)
    regexp = get_regexp(language)
    if not regexp:
        return binary_text
    return regexp.sub(b'\n', text)

def get_regexp(language):
    re_inline = get_inline(language)
    re_block = get_block(language)
    rs = []
    if re_inline:
        rs.append(re_inline)
    if re_block:
        rs.append(re_block)
    if rs == []:
        return None
    return re.compile(b'|'.join(rs), re.DOTALL)
        
    
def get_inline(language):
    r_base = b'[^\\n]*(?:\\n|$)'
    if language in ['Ada',
                    'Eiffel',
                    'VHDL',
                    'AppleScript',
                    'Haskell',
                    'Lua',
                    'PLSQL']:
        r = b'(--)' + r_base
    elif language in ['C',
                      'C++',
                      'C#',
                      'D',
                      'JavaScript',
                      'ActionScript',
                      'Java',
                      'Rust']:
        r = b'(//)' + r_base
    elif language == 'Xojo':
        r = b'(' + b'|'.join([b'//', b"\'"]) + b')' + r_base
    elif language in ['R',
                      'Tcl',
                      'Awk',
                      'Perl',
                      'Perl 6',
                      'Ruby',
                      'Python']:
        r = b'(#)' + r_base
    elif language in ['COBOL']:
        r = b'(\\*>)' + r_base
    elif language in ['Matlab']:
        r = b'(%)' + r_base
    else:
        return None
    return b'(' + r + b')'

def get_block(language):
    r_base = b'.*?'
    if language in ['C',
                    'C++',
                    'C#',
                    'JavaScript',
                    'ActionScript',
                    'PLSQL',
                    'PHP',
                    'Rust']:
        r = b'(/\\*)' + r_base + b'(\\*/)'
    elif language in ['OCaml',
                      'Pascal',
                      'Modula-2',
                      'Smarty']:
        r = b'(\\(\\*)' + r_base + b'(\\*\\))'
    elif language == 'Python':
        r = b'(\'\'\')' + r_base + b'(\'\'\')'
    else:
        return None
    return b'(' + r + b')'
    
    
def purify(text, lang):
    # TODO: for some language like HTML, remove code other than principal language
    pass



    
    
    
    

